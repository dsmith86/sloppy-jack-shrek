import discord
import asyncio
import os
import argparse
from repoze.lru import LRUCache
import re
from xml.etree import ElementTree
import requests
import random
import pprint
from io import BytesIO

from modules.util import action
from modules.config.config import ConfigManager
from modules.commands import commands
from modules.reactions import reactions
from modules.gui.gui import GuiManager
from modules.gw2api import apikey
from modules.gw2api import raids
from modules.shots import shots
from modules.response import response_builder

config_manager = ConfigManager.instance()

client = discord.Client()

cache = LRUCache(300)

gui = GuiManager.instance()

async def process_instructions(message, instructions):
	if not instructions:
		return

	async for response in response_builder.build_response(message, instructions):	
		output = ""
		for response_set in response:
			response_components = []
			if isinstance(response_set, dict):
				response_components.append(response_set)
			else:
				response_components = response_set

			for response_component in response_components:
				if response_component["type"] == "raw":
					output += response_component["contents"]
				else:
					if len(output) > 0:
						await action.process(client=client, message=message, type="raw", component="{}".format(output))
						output = ""
					extras = {
						"cache": cache
					}
	
					await action.process(client=client, message=message, type=response_component["type"], component=response_component, extras=extras)
		if len(output) > 0:
			await action.process(client=client, message=message, type="raw", component="{}".format(output))

async def process_talk(message):
	print(message.author)
	print(message.attachments)
	print(message.content)
	channel = ""
	try:
		channel, m = message.content.split(' ', 1)
	
		channel = channel[2:][:-1]
		await client.send_message(client.get_channel(channel), message.content.split(' ', 1)[1])

		
	except ValueError as e:
		channel = message.content[2:][:-1]

	for a in message.attachments:
		url = a['url']
		response = requests.get(url)
		data = BytesIO(response.content)
		
		await client.send_file(client.get_channel(channel), data, filename=a['filename'])

	await client.delete_message(message)

@client.event
async def on_typing(channel, user, when):
	if channel.id == "400403475239927818":
		last_message_id = cache.get("last-message")
		await client.send_typing(client.get_channel(last_message_id))

@client.event
async def on_ready():
	print('Logged in as')
	print(client.user.name)
	print(client.user.id)
	print('------')
	print(discord.__version__)

@client.event
async def on_message(message):
	if message.author.id == "295982649133236224":
		return

	if message.author == client.user:
		key = "last-message-{}".format(message.channel.id)
		cache.put(key, message.id)

	ignore = False

	with open("data/identity") as f:
		identity = f.read().rstrip()
		if identity != 'alpha' and message.author.id != '289849603430416384':
			ignore = True

	if message.channel.id == "400403475239927818" and not ignore:
		await process_talk(message)
		return

	if config_manager.respond_channel(message.channel.id) and message.author != client.user and not ignore:
		cache.put("last-message", message.channel.id)

		commands_used = 0

		instructions = await commands.process_message(message, client)

		await process_instructions(message, instructions)


		for mention in message.mentions:
			if mention == client.user:
				commands_used += 1		
				with open('data/images/shrek.jpg', 'rb') as f:
					await client.send_file(message.channel, f)
		if message.author.id == '138633439204212736':
			await client.add_reaction(message, discord.Emoji(name="whiteflag", id="278739292329738242", server="254462784027361280"))
		if message.content.lower().startswith("shots"):
			commands_used += 1
			response = shots.process_command(message.author.id, message.content.lower())

			if response:
				await client.send_message(message.channel, response)
		if apikey.matches_api_pattern(message.content):
			commands_used += 1
			try:
				apikey.set_api_key(message.author.id, message.content)
				await client.send_message(message.channel, "Saved that API key fam :ok_hand:")
				if "delete" in message.content.lower():
					await client.delete_message(message)
			except apikey.InvalidAPIKeyException as e:
				await client.send_message(message.channel, "Check dat API key son, shits on fire yo")
		if "aye fam lemme see them raid buys" in message.content.lower():
			commands_used += 1
			try:
				await client.send_message(message.channel, raids.get_raid_progress(message.author.id).format(message.author.mention))
			except apikey.NoAPIKeyAvailableException as e:
				await client.send_message(message.channel, "You need to give me an API key dawg")

		commands_used += len(instructions)

		if commands_used >= 3 and random.choice([True, False]):
			api_key = os.environ['CAT_API_KEY']

			r = requests.get('http://thecatapi.com/api/images/get?format=xml&results_per_page=20&api_key={}'.format(api_key))

			tree = ElementTree.fromstring(r.content)

			images = tree.find('data').find('images')

			response = ""
			
			for image in images:
				response += "{}\n".format(image.find('url').text)
			
			await client.send_message(message.author, response)

@client.event
async def on_reaction_add(reaction, user):
	with open("data/identity") as f:
		identity = f.read().rstrip()
		if identity == 'beta':
			return

	if config_manager.respond_channel(reaction.message.channel.id):

		instructions = await reactions.process_reaction(reaction, user, client, {"cache": cache})

		await process_instructions(reaction.message, instructions)

with open("data/identity") as f:
	identity = f.read().rstrip()
	if identity == 'alpha':
		client.run(os.environ['SLOPPY_JACK_BOT_TOKEN'])
	else:
		client.run(os.environ['SLOPPY_JACK_BOT_TOKEN_{}'.format(identity)])
