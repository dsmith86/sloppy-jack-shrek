import re

def substring_indices(substring, string):
	index = 0

	while index < len(string):
		index = string.find(substring, index)
		if index == -1:
			break

		yield index		

		index += len(substring)

def retrieve_word_at_index(string, index):
	pattern = re.compile(r"\w*([\w|\-]*)")
	prefix = ""

	print(string[::-1])
	print(len(string) - index)

	try:
		prefix = (pattern.search(string[::-1], len(string) - index).group(0))
	except AttributeError as no_prefix_error:
		pass

	return prefix[::-1] + pattern.search(string, index).group(0)
