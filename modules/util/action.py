import discord
import math
import time

TYPE_DURATION = 10

def action_send_message(client, channel = None, message = None):
	return client.send_message(channel, message)

def action_react(client, message = None, reaction = None):
	return client.add_reaction(message, reaction)

async def action_delay(client, message, duration, send_type):
	iterations = math.floor(duration / TYPE_DURATION + 1)

	if duration > 0:
		for i in range(0, iterations):
			if send_type:
				await client.send_typing(message.channel)
			time.sleep(min(TYPE_DURATION, max(duration - i * TYPE_DURATION, 0)))
	return client.application_info()

async def action_edit_last_comment(client, message, cache, contents):
	last_message = cache.get('last-message-{}'.format(message.channel.id))
	message_to_edit = await client.get_message(message.channel, last_message)

	await client.edit_message(message_to_edit, contents)

async def action_edit_comment(client, message, cache, contents, id):
	cache_key = "edit_{}".format(id)

	edit_message = cache.get(cache_key)

	if edit_message is None:
		edit_message = await client.send_message(message.channel, contents)
		cache.put(cache_key, edit_message)
	else:
		await client.edit_message(edit_message, contents)

async def action_send_image(client, message, cache, path):
	with open("data/images/{}".format(path), 'rb') as f:
		await client.send_file(message.channel, f)	

async def action_send_notification(client, message, destination, message_to_send):
	pass

async def process(client, message, type, component, extras = {}):
	print(type)
	if type == "react":
		await action_react(client, message, component["reaction"])
	elif type == "delay":
		await action_delay(client, message, component["duration"], component["send_type"])
	elif type == "action_edit_last_comment":
		await action_edit_last_comment(client, message, extras["cache"], component["contents"])
	elif type == "edit_comment":
		await action_edit_comment(client, message, extras["cache"], component["contents"], component["uuid"])
	elif type == "image_local":
		await action_send_image(client, message, extras["cache"], component["path"])
	elif type == "notification":
		await action_send_notification(client, message, component["destination"], component["message"])
	else:
		await action_send_message(client, message.channel, component)
