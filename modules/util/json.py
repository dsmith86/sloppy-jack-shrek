import json

def json_list(path, schema = None):
	with open(path) as d:
		json_list = json.load(d)

		for i, item in enumerate(json_list):
			json_list[i] = dotnotation(item)

		return json_list

def json_dict(path, schema = None):
	with open(path) as d:
		json_dict = json.load(d)

		return dotnotation(json_dict)

class dotnotation(dict):
	__getattr__ = dict.get
	__setattr__ = dict.__setitem__
	__delattr__ = dict.__delitem__
