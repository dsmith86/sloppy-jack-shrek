from modules.commands.parsers import parser
from modules.util.search import substring_indices
import re

def parse_wordlist(triggers, contents, command):
	for trigger in triggers:
		for index in substring_indices(trigger, contents):
			yield index

def parse_vars(trigger, contents, command):
	vars = re.findall(r'{(\w[\w|\d]*)}', trigger)

	pattern = re.sub(r'{[%s]}' % '|'.join(vars), r'(.+)', trigger)	

	try:
		var_vals = re.findall(pattern, contents)[0]
		
		try:
			command["response_builder"]["extras"]
		except KeyError as no_extras_error:
			command["response_builder"]["extras"] = {}
		command["response_builder"]["extras"]["vars"] = dict(zip(vars, var_vals))
	except IndexError as no_match_error:
		pass	

	try:
		yield re.search(pattern, contents).start()
	except AttributeError as no_match_error:
		pass

class SimpleKeywordsParser(parser.Parser):
	def __init__(self, contents):
		super(SimpleKeywordsParser, self).__init__(contents)
		self._retrieve_command_list('simple_keywords')		

		self._command_parsers = {**self._command_parsers, **{
			"word_list": parse_wordlist,
			"vars": parse_vars
		}}
