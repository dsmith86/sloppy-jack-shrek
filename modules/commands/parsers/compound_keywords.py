from modules.commands.parsers import parser
from modules.util.search import substring_indices

def parse_expression(triggers, contents, command):	
	indices_found = []
	set_indices = []

	while True:
		index = 0
		for trigger in triggers:
			while True:
				index = contents.find(trigger, index)

				if index == -1:
					return set_indices

				if index in indices_found:
					index += 1
				else:
					indices_found.append(index)
					index = 0
					break
		set_indices.append(indices_found[-1])

class CompoundKeywordsParser(parser.Parser):
	def __init__(self, contents):
		super(CompoundKeywordsParser, self).__init__(contents)
		self._retrieve_command_list('compound_keywords')		

		self._command_parsers = {**self._command_parsers, **{
			"compound": parse_expression
		}}
