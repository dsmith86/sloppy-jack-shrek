from modules.commands.parsers import parser
from modules.util.search import substring_indices

def parse_mention(trigger, contents, command):
	total_indices = []

	for index in substring_indices(trigger, contents):
		total_indices.append(index)

	for index in total_indices:
		yield index

class MentionsParser(parser.Parser):
	def __init__(self, contents):
		super(MentionsParser, self).__init__(contents)
		self._retrieve_command_list('mentions')		

		self._command_parsers = {**self._command_parsers, **{
			"mention": parse_mention
		}}

