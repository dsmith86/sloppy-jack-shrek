from abc import ABC, abstractmethod
import re
import string
import reprlib

from modules.util.json import json_list
from modules.util.search import substring_indices

def parse_simple(trigger, contents, command):
	for index in substring_indices(trigger, contents):
		yield index

def parse_regex(pattern, contents, command):
	iter = re.finditer(r"{}".format(pattern), contents)

	for index in iter:
		yield index.start(0)

def parse_terminate(trigger, contents, command):
	for index in parse_regex(trigger, contents, command):
		yield 0
		raise Exception("Terminating")

class Parser(ABC):
	_contents_raw = ""
	_contents_lower = ""
	_command_list = []

	_command_parsers = {
		"simple": parse_simple,
		"regex": parse_regex,
		"terminate": parse_terminate
	}

	@abstractmethod
	def __init__(self, contents):
		self._contents_raw = contents
		self._contents_lower = contents.lower()

	def _retrieve_command_list(self, path):
		self._command_list = json_list("data/commands/{}.json".format(path))

	def _is_partial(self, trigger, contents, index):
		pattern_string = "\\b{}\\b".format(trigger)
		pattern = re.compile(pattern_string)
		match = pattern.search(contents, index)
		if match:
			return False
		return True

	def parse(self):
		results = []
		unique_triggers = {}

		for command in self._command_list:
			try:
				for index in self._command_parsers[command.parse_type](command.trigger, self._contents_lower, command):
					trigger_key = reprlib.repr(command.trigger)

					if not command.partial and self._is_partial(command.trigger, self._contents_lower, index):
						continue
					try:
						if not command.spammable and unique_triggers[trigger_key]:
							continue
					except KeyError as not_registered_error:
						pass
					if command.leading_only and index > 0:
						continue

					s = self._contents_lower.translate(self._contents_lower.strip().maketrans('', '', string.punctuation))

					if command.exclusive and eval(trigger_key) != s:
						continue
					results.append({
						"response_builder": command.response_builder,
						"index": index
					})
					unique_triggers[trigger_key] = True
			except KeyError as command_type_not_implemented_error:
				pass

		return results

