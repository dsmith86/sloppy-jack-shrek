import asyncio
from operator import itemgetter

from modules.commands.parsers.simple_keywords import SimpleKeywordsParser
from modules.commands.parsers.compound_keywords import CompoundKeywordsParser
from modules.commands.parsers.mentions import MentionsParser

async def process_message(message, client):
	message_contents = message.content.lower()
	occurrences = []

	occurrences.extend(SimpleKeywordsParser(message_contents).parse())
	occurrences.extend(CompoundKeywordsParser(message_contents).parse())
	occurrences.extend(MentionsParser(message_contents).parse())

	occurrences = sorted(occurrences, key=itemgetter('index'))

	return occurrences

