import asyncio

from modules.util.json import json_list

async def process_reaction(reaction, user, client, extras):
	reaction_list = json_list("data/reactions/reactions.json")

	if reaction.custom_emoji:
		pass
	else:
		for reaction_item in reaction_list:
			try:
				print(reaction.emoji)
				print(reaction_item)
				if reaction.emoji == reaction_item["emoji"]:
					key = "{0}:{1}".format(reaction_item["emoji"], reaction.message.id)
					cache = extras["cache"]
					
					if cache.get(key) is None:
						cache.put(key, True)
						return [{
							"response_builder": reaction_item["response_builder"]
						}]
					return []

			except KeyError as not_base_emoji_error:
				pass
