
from modules.gw2api import apikey
import requests
import json

class RaidCollection(object):
	def __init__(self):
		self.raids = []

	def add_raid(self, raid):
		self.raids.append(raid)

	def get_status(self):
		response = "The scrubu known as {} has bought the following encounters:\n\n"

		completed = 0
		total = 0

		for i, raid in enumerate(self.raids):
			for j, wing in enumerate(raid.wings):
				for k, encounter in enumerate(wing.encounters):
					if encounter.completed:
						completed += 1
					total += 1

					encounter_name = " ".join(encounter.id.split("_")).title()
					status_completed = "Bought" if encounter.completed else "Too poor to buy"

					bold_if_completed = "**" if encounter.completed else ""

					response += "{3}{4}{3}: {5}\n".format(i + 1, j + 1, k + 1, bold_if_completed, encounter_name, status_completed)

		response += "\nThat's {0} out of {1} encounters. ".format(completed, total)

		if completed / total < 0.25:
			response += "Wow what a scrubu confirmed."
		elif completed / total < 0.50:
			response += "Not bad for a scrubu"
		elif completed / total < 0.75:
			response += "You are aware you're gonna have to buy 150 of these?"
		else:
			response += "RIP your credit card lmao"

		return response

class Raid(object):
	def __init__(self, id):
		self.wings = []
		self.id = id

	def add_wing(self, wing):
		self.wings.append(wing)

class Wing(object):
	def __init__(self, id):
		self.encounters = []
		self.id = id

	def add_encounter(self, encounter):
		self.encounters.append(encounter)

class Encounter(object):
	def __init__(self, id, type):
		self.id = id
		self.type = type
		self.completed = False

	def set_completed(self):
		self.completed = True

def process_raid_encounters(raid_collection, encounters):
	with open('data/raids/raids.json') as json_data:
		raids = json.load(json_data)

		for raid_id in raids:
			raid_obj = Raid(raid_id)
			raid_collection.add_raid(raid_obj)

			with open('data/raids/raids_{}.json'.format(raid_id)) as json_data:
				wings = json.load(json_data)

				for wing in wings['wings']:
					wing_obj = Wing(wing['id'])
					raid_obj.add_wing(wing_obj)

					for encounter in wing['events']:
						encounter_obj = Encounter(encounter['id'], encounter['type'])
						wing_obj.add_encounter(encounter_obj)

						if encounter_obj.id in encounters:
							encounter_obj.set_completed()

def get_raid_progress(id):
	raid_collection = RaidCollection()	

	api_key = apikey.get_api_key(id)

	if api_key is None:
		raise apikey.NoAPIKeyAvailableException('No API Key Found.')		

	try:
		r = requests.get("https://api.guildwars2.com/v2/account/raids?access_token={}".format(api_key))

		process_raid_encounters(raid_collection, r.json())

		return raid_collection.get_status()
	except Exception as e:
		print(e)

