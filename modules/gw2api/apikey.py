from modules.util import storage
import re
import requests

API_KEY_PATTERN = r'[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{20}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}'

class ApiKeyException(Exception):
	"""Base class for Api Key Exceptions"""
	pass

class InvalidAPIKeyFormatException(ApiKeyException):
	"""Exception raised for incorrect API key formatting"""
	pass

class IncorrectAPIPermissionsException(ApiKeyException):
	"""Exception raised for API Keys that don't have the correct permissions"""
	pass

class APIRequestException(ApiKeyException):
	"""Exception raised when API requests fail"""
	pass

class NoAPIKeyAvailableException(ApiKeyException):
	"""Exception raised when a user doesn't have an API key associated"""
	pass

class InvalidAPIKeyException(ApiKeyException):
	"""Exception raised for an invalid API key"""
	pass

def matches_api_pattern(s):
	regexp = re.compile(API_KEY_PATTERN)

	return regexp.search(s)

def api_key_is_valid(key):
	r = requests.get("https://api.guildwars2.com/v2/account?access_token={}".format(key))

	return 'name' in r.json()

def key_from_string(s):
	regex = re.compile(".*({})".format(API_KEY_PATTERN))
	return regex.findall(s)[0]

def set_api_key(id, key):
	if matches_api_pattern(key):
		if api_key_is_valid(key_from_string(key)):
			storage.set_user_attribute(id, 'api_key', key_from_string(key))
		else:
			raise InvalidAPIKeyException('API Key is invalid.')
	else:
		raise InvalidAPIKeyFormatException('API Key is malformed.')

def get_api_key(id):
	try:
		return storage.get_user_attribute(id, 'api_key')
	except Exception as e:
		pass
