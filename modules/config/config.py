import argparse

from modules.util.singleton import Singleton
from modules.util.json import json_dict

class ConfigManager(Singleton):
	def __init__(self):
		parser = argparse.ArgumentParser(description = '[EJAC] Discord Bot')
		parser.add_argument('-q', '--qa', action='store_const', const=True, default=False, help='QA-ready version of the bot.')
		parser.add_argument('-d', '--dev', action='store_const', const=True, default=False, help='Development-ready version of the bot.')
		parser.add_argument('-c', '--comment', action='store', default=False, help='Add a comment.')
		self.args = parser.parse_args()

		var_dict = vars(self.args)
		channel_info = json_dict('data/config/channel_info.json')
		
		self.valid_channels = []

		for env, channel_list in (channel_info["channels"]).items():
			if not (self.args.qa or self.args.dev) or var_dict[env]:
				for channel in channel_list:
					self.valid_channels.append(channel)

	def respond_channel(self, channel_id):
		if self.args.qa or self.args.dev:
			return channel_id in self.valid_channels
		return channel_id not in self.valid_channels
