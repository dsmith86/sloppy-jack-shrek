from modules.util import storage
import discord
from operator import itemgetter

def process_command(id, command):
	if command == "shots":
		response = ""
		shots_so_far = 0
		try:
			shots_so_far = int(storage.get_user_attribute(id, "shots"))
		except KeyError as e:
			response = "Come here often? "
		finally:
			shots_so_far += 1
			storage.set_user_attribute(id, "shots", shots_so_far)

		return "{0}Shots so far: {1}".format(response, shots_so_far)
	if "reset" in command:
		storage.set_user_attribute(id, "shots", 0)

		return "Reset your shot count to 0, you weak scrubu tryhard"
	if "leaderboard" in command:
		leaderboard = storage.get_attribute_for_users("shots")

		leaderboard = sorted(leaderboard, key=itemgetter('shots'), reverse=True)
		
		response = "**Shots per scrubu:**\n\n"

		for i, user in enumerate(leaderboard):
			response = "{0}**#{1}**: <@{2}>: {3} :whisky:\n".format(response, i + 1, user['id'], user['shots'])

		return response
	if "fired" in command:
		return "PEW PEW PEW"
	if "forget" in command:
		try:
			storage.remove_user_attribute(id, "shots")
			return "Who are you again?"
		except KeyError as e:
			return "lol I don't even know you"
