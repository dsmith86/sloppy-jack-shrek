import asyncio
import discord
import time
import uuid
import random
import traceback

from modules.util.json import json_dict
from modules.util.search import retrieve_word_at_index

def respond_text(message, response_component):
	return {
		"type": "raw",
		"contents": response_component["contents"]
	}

def respond_link(message, response_component):
	response = {
		"type": "raw"
	}

	if response_component["embed"] == True:
		response["contents"] = response_component["contents"]
	else:
		response["contents"] = "<{}>".format(response_component["contents"])

	return response

def respond_mention(message, response_component):
	return {
		"type": "raw",
		"contents": message.author.mention
	}

def respond_id_mention(message, response_component):
	return {
		"type": "raw",
		"contents": message.server.get_member(response_component["id"]).mention
	}

def respond_custom_emoji(message, response_component):
	return {
		"type": "raw",
		"contents": "<:{0}:{1}>".format(response_component["name"], response_component["id"])
	}

def respond_reaction_source(message, response_component):
	if response_component["reaction_type"] == "default":
		return {
			"type": "react",
			"reaction": response_component["name"]
		}
	elif response_component["reaction_type"] == "custom":
		return {
			"type": "react",
			"reaction": discord.Emoji(name=response_component["name"], id=response_component["id"], server=message.server)
		}

def respond_edit_last_comment(message, response):
	return {
		"type": "action_edit_last_comment",
		"contents": response["contents"]
	}

def respond_quote_word(message, response):
	contents = message.content
	word = retrieve_word_at_index(contents, response["extras"]["trigger_index"])
	
	return {
		"type": "raw",
		"contents": "```css\n> {}\n```\n".format(word)
	}

def respond_random(message, response):
	random.seed()
	choice = random.choice(response["options"])
	choice["extras"] = response["extras"]

	return responders[choice["type"]](message, choice)

def respond_var(message, response):
	print(response)

	return responders["text"](message, {
		"contents": response["extras"]["vars"][response["name"]]
	})

def respond_group(message, response):
	aggregate_response = []

	for step in response["steps"]:
		step["extras"] = response["extras"]
		aggregate_response.append(responders[step["type"]](message, step))

	return aggregate_response

def respond_image(message, response):
	if response["location"] == "local":
		return {
			"type": "image_local",
			"path": response["path"]
		}

def respond_notification(message, response):
	return {
		"type": "notification",
		"destination": response["destination"],
		"message": response["message"]
	}

responders = {
	"text": respond_text,
	"link": respond_link,
	"author_mention": respond_mention,
	"id_mention": respond_id_mention,
	"custom_emoji": respond_custom_emoji,
	"reaction_source": respond_reaction_source,
	"action_edit_last_comment": respond_edit_last_comment,
	"quote_word": respond_quote_word,
	"random": respond_random,
	"var": respond_var,
	"group": respond_group,
	"image": respond_image,
	"notification": respond_notification
}

def process_standard(message, response, steps_key = "standard_steps"):
	response_components = []

	steps = response[steps_key]

	for step in steps:
		step["extras"] = response["extras"]

		type = step["type"]
		try:
			s = responders[type](message, step)
			if s:
				response_components.append(s)
		except KeyError as e:
			print("Unsupported response type '{}'".format(type))
			traceback.print_exc()

	return response_components

def process_condition_author(message, response):
	potential_matches = json_dict("data/person_ids.json")

	step_type = ""

	try:
		named_steps = "{}_steps".format(potential_matches[message.author.id])
		if response[named_steps]:
			step_type = named_steps
	except KeyError as e:
		step_type = "standard_steps"

	return process_standard(message, response, step_type)

def process_post_sequence(message, response):
	sequence_steps = response["sequence_steps"]

	response_components = []

	for step in sequence_steps:
		step["extras"] = response["extras"]

		if step["delay"] > 0:
			response_components.append({
				"type": "delay",
				"duration": step["delay"],
				"send_type": step["send_type"]
			})
		response_components.extend(process_standard(message, step, "step_components"))

	return response_components

def process_edit_sequence(message, response):
	sequence_steps = response["sequence_steps"]

	response_components = []

	id = uuid.uuid4()

	for step in sequence_steps:
		step["extras"] = response["extras"]

		if step["delay"] > 0:
			response_components.append({
				"type": "delay",
				"duration": step["delay"],
				"send_type": False
			})
		response_component = process_standard(message, step, "step_components")

		combined_component = [
			{
				"type": "edit_comment",
				"uuid": id,
				"contents": ""
			}]

		for component_piece in response_component:
			if (component_piece["type"] == "raw"):
				combined_component[0]["contents"] += component_piece["contents"]
			else:
				response_components.append(component_piece)

		response_components.extend(combined_component)

	return response_components

processors = {
	"standard": process_standard,
	"condition_author": process_condition_author,
	"post_sequence": process_post_sequence,
	"edit_sequence": process_edit_sequence
}

async def build_response(message, response_data):
	for response in response_data:
		#TODO: Refactor
		try:
			response["response_builder"]["extras"]
		except KeyError as no_extras_error:
			response["response_builder"]["extras"] = {}

		try:
			response["response_builder"]["extras"]["trigger_index"] = response["index"]
		except KeyError as no_index_error:
			pass

		type = response["response_builder"]["type"]
		try:
			yield processors[type](message, response["response_builder"])
		except KeyError as e:
			print("Unsupported processor type '{}'".format(type))
			traceback.print_exc()
